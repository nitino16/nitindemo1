package com;

public class Address {
	private  int addrId;
	private String city;
    private String state;
    private Student s;
	public int getAddrId() {
		return addrId;
	}
	public void setAddrId(int addrId) {
		this.addrId = addrId;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Student getS() {
		return s;
	}
	public void setS(Student s) {
		this.s = s;
	}

}
