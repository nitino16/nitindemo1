package com;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Test {
public static void main(String[] args) {
	Configuration configuration=new Configuration().configure("com/sa.cfg.xml");
	  SessionFactory sessionFactory=configuration.buildSessionFactory();
	  Session session=sessionFactory.openSession();
	  Transaction transaction=session.beginTransaction();
	  
  
	  Student student=new Student();
	  student.setStudId(102);
	  student.setStudName("cjc");
	 
	  Address address=new Address();
	  
	  address.setAddrId(102);
	  address.setCity("pune");
	  address.setState("maharashtra");
	  address.setS(student);
	// student.setAddress(address);
	 
	
	  
	  session.save(address);
	  /*Object o=session.get(Student.class, new Integer(1));
	  Student stu=(Student) o;
	  System.out.println(stu.getStudId()+"    "+stu.getStudName()+"   "+stu.getAddress().getAddrId()+"    "+stu.getAddress().getCity()+"    "+stu.getAddress().getState());
	  */
	  /*Object o=session.get(Address.class, new Integer(1));
	  Address addr=(Address) o;
	  System.out.println(addr.getAddrId()+""+addr.getCity()+""+addr.getState()+""+addr.getS().getStudId()+""+addr.getS().getStudName());*/
	  // iteration all values
	 /* Query query=session.createQuery("from Address");
	  List<Address> l=query.list();
	  Iterator itr=l.iterator();
	  while(itr.hasNext())
	  {
		  Address addr=(Address) itr.next();
		  System.out.println(addr.getAddrId()+""+addr.getCity()+""+addr.getState()+""+addr.getS().getStudId()+""+addr.getS().getStudName());
	  }
	  */
	  transaction.commit();
	  session.close();
	  System.out.println("one to one done bidirectional");
}
}
